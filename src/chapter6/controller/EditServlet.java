package chapter6.controller;

import java.io.IOException;
import java.rmi.ServerException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = {"/edit"})
public class EditServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServerException, ServletException {
		String messageId = request.getParameter("id");
		List<String> errorMessages = new ArrayList<>();
		Message message = null;
		if(messageId != null && messageId.matches("^[0-9]+$")) {
			message = new MessageService().confirmMessage(messageId);
		}
		if(message == null) {
			errorMessages.add("不正なパラメータが入力されました");
			request.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./");
			return;
		}
		request.setAttribute("message", message);
		request.getRequestDispatcher("edit.jsp").forward(request, response);
	}

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    		throws IOException, ServletException {

    	String id = request.getParameter("id");
    	String text = request.getParameter("text");
    	HttpSession session = request.getSession();
    	List<String> errorMessages = new ArrayList<String>();

    	Message message = new Message();
    	message.setText(text);
    	message.setId(Integer.parseInt(id));

    	if (!isValid(text, errorMessages)) {
    		session.setAttribute("errorMessages", errorMessages);
    		session.setAttribute("message", message);
    		request.getRequestDispatcher("edit.jsp").forward(request, response);
    		return;
    	}

    	new MessageService().edit(message);
    	response.sendRedirect("./");
	}

    private boolean isValid(String text, List<String> errorMessages) {

        if (StringUtils.isBlank(text)) {
            errorMessages.add("入力してください");
        } else if (140 < text.length()) {
            errorMessages.add("140文字以下で入力してください");
        }

        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }

}

